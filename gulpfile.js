// grab our packages
const gulp   = require('gulp');
const gls    = require('gulp-live-server');
const jshint = require('gulp-jshint');
const mocha  = require('gulp-mocha');
const exec   = require('child_process').exec;

let paths = {
    src  : ['server.js','app/**/*.js'],
    test : ['test/**/*.js']
};

let server;

// define the default task and add the serve task to it
gulp.task('default', ['serve']);

// configure the jshint task
gulp.task('jshint', function() {
    return gulp.src(paths.src)
    .pipe(jshint({esversion: 6}))
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('restart-server', ['jshint'], function(){
    if(server){
        server.start.bind(server)();
    }
});

gulp.task('serve', ['jshint'], function() {
    //1. run your script as a server 
    server = gls.new('server.js');
    server.start();
    
    //use gulp.watch to trigger server actions(notify, start or stop) 
    gulp.watch(paths.src, ['restart-server']);
});


gulp.task('seed', function(cb){
    exec('node seeder/index.js', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});


gulp.task('test', function() {
    process.env.NODE_ENV = 'test';
    return gulp.src(paths.test)
    .pipe(jshint({esversion: 6, expr: true}))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(mocha({ reporter: 'spec', exit: true, timeout: 10000}));
});

gulp.task('watch:test', ['test'], function(){
    gulp.watch([paths.src, paths.test], ['test']);
})