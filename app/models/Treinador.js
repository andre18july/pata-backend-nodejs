const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const treinadorSchema = new Schema({

    user:{
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    treinos: [{
        type: Schema.Types.ObjectId,
        ref: 'treino'
    }],
    credencial: {
        type: String,
        required: true
    },
    morada: {
        type: String
    },
    contacto: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }

});

const Treinador = mongoose.model('treinador', treinadorSchema);
module.exports = Treinador;