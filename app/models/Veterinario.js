const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const veterinarioSchema = new Schema({

    user:{
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    credencial: {
        type: String,
        required: true
    },
    especialidade: {
        type: String,

    },
    horario: {
        type: String,

    },
    morada: {
        type: String
    },
    contacto: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }

});

const Veterinario = mongoose.model('veterinario', veterinarioSchema);
module.exports = Veterinario;