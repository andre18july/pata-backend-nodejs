const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const uniqueValidator = require('mongoose-unique-validator');
const timestamps = require('../components/timestamps');

var UserCompletoSchema = new Schema({
    userid: {
        type: String,
    },
    name: {
        type: String,
        required: [true, "O `nome` é obrigatório"]
    },
    email: {
        type: String,
        required: [true, "O `email` é obrigatório"],
        index: true,
        unique: true
    },
    role: {
        type: String,
        required: true,
        enum: ['admin', 'responsavelanimais', 'veterinario', 'treinador']
    },
    ativo: {
        type: Boolean,
        default: true
    },
    credencial: {
        type: String,
        required: true
    },
    contacto: {
        type: String
    }


});

const UserCompleto = mongoose.model('usercompleto', UserCompletoSchema);
module.exports = UserCompleto;