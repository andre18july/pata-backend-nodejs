const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventoRespAnimaisSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    veterinario: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    nomeVeterinario: {
        type: String
    },
    animal: {
        type: Schema.Types.ObjectId,
        ref: 'animal',
        required: true
    },
    nomeAnimal: {
        type: String
    },
    tipo: {
        type: String
    },
    observacoes: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }
});

const EventoRespAnimais = mongoose.model('eventoRespAnimais', eventoRespAnimaisSchema);
module.exports = EventoRespAnimais;