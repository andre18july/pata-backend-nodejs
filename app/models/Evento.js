const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventoSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    veterinario: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    animal: {
        type: Schema.Types.ObjectId,
        ref: 'animal',
        required: true
    },
    tipo: {
        type: String
    },
    observacoes: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }
});

const Evento = mongoose.model('evento', eventoSchema);
module.exports = Evento;