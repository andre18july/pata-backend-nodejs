const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const intervencaoSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    evento: {
        type: Schema.Types.ObjectId,
        ref: 'evento',
        required: true
    },
    descricao: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    observacoes: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }
});

const Intervencao = mongoose.model('intervencao', intervencaoSchema);
module.exports = Intervencao;