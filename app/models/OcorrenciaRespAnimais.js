const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ocorrenciaRespAnimaisSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    tipo: { //acidente, perda, maus tratos
        type: String,
        required: true,
        enum: ['acidente', 'perda', 'maustratos']
    },
    animal: {
        type: Schema.Types.ObjectId,
        ref: 'animal',
    },
    nomeAnimal: {
        type: String
    },
    responsavelAnimais: {
        type: Schema.Types.ObjectId,
        ref: 'user',
    },
    descricao: {
        type: String,
        required: true
    },
    localizacao: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    long: {
        type: String,
        required: true
    },
    estadoAnimal: {
        type: String,
        enum: ['ferido', 'morto','']
    },
    tipoAnimal: {
        type: String
    },
    racaAnimal: {
        type: String
    },
    fotoAnimal: {
        type: String
    },
    contacto:{
        type: String,
        required: true
    },
    agressor: {
        type: String
    },
    notificada: {
        type: Boolean,
        default: false
    },
    ativo: {
        type: Boolean,
        default: true
    },

});

const OcorrenciaRespAnimais = mongoose.model('ocorrenciaRespAnimais', ocorrenciaRespAnimaisSchema);
module.exports = OcorrenciaRespAnimais;