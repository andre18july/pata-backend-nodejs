const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const treinoSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    treinador: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    animal: {
        type: Schema.Types.ObjectId,
        ref: 'animal',
        required: true
    },
    tipo: {
        type: String
    },
    desempenho: {
        type: String
    },
    observacoes: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }
});

const Treino = mongoose.model('treino', treinoSchema);
module.exports = Treino;