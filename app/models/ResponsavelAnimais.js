const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const responsavelAnimaisSchema = new Schema({

    user:{
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    animais: [{
        type: Schema.Types.ObjectId,
        ref: 'animal'
    }],
    ativo: {
        type: Boolean,
        default: true
    }
});

const ResponsavelAnimais = mongoose.model('responsavelAnimais', responsavelAnimaisSchema);
module.exports = ResponsavelAnimais;