const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const animalSchema = new Schema({
    nome: String,
    tipo: String,
    raca: String,
    genero: {
        type: String,
        required: true
    },
    idade: String,
    ativo: {
        type: Boolean,
        default: true
    },
    responsavel: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    fotoAnimal: {
        type: String
    },
    treinos: [{
        type: Schema.Types.ObjectId,
        ref: 'treino'
    }],
});

const Animal = mongoose.model('animal', animalSchema);
module.exports = Animal;