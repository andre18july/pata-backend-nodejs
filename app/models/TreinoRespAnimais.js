const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const treinoRespAnimaisSchema = new Schema({
    data : {
        type: Date,
        default: Date.now,
        required: true
    },
    treinador: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    nomeTreinador: {
        type: String
    },
    animal: {
        type: Schema.Types.ObjectId,
        ref: 'animal',
        required: true
    },
    nomeAnimal: {
        type: String
    },
    tipo: {
        type: String
    },
    desempenho: {
        type: String
    },
    observacoes: {
        type: String
    },
    ativo: {
        type: Boolean,
        default: true
    }
});

const TreinoRespAnimais = mongoose.model('treinoRespAnimais', treinoRespAnimaisSchema);
module.exports = TreinoRespAnimais;