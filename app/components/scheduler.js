const scheduler = require('node-schedule');
const socket = require('./websocket');
const mailer = require('./mailer');
const User = require('../models/User');
const moment = require('moment');
const Evento = require('../models/Evento');
const Veterinario = require('../models/Veterinario');
const nodemailer = require('nodemailer');

function begin() {
    //scheduler.scheduleJob('* * * * * *', watchAlert) // todos os segundos
    scheduler.scheduleJob('0 0 6 * * *', watchAlert) // todos os dias as 6:00 am
    //watchAlert()
}


async function watchAlert(){
    try{
        console.log("CRON");
        //obter todos os eventos ativos no sistema
        const eventos = await Evento.find({}).populate("animal");
        console.log("Eventos: ", eventos);
    
        //percorrer os eventos
        for(var i=0;i<eventos.length;i++){

            //verifica se o evento esta ativo
            if(eventos[i].ativo === true){
                //verificar se a data dos eventos é <= que data atual mais 3 dias
                var today = new Date();
                var newdate = new Date();
                newdate.setDate(today.getDate()+3);


                //console.log("newdate: ", newdate);
                if(eventos[i].data <= newdate){
                    //obtem o veterinario
                    const vet = await User.findById(eventos[i].veterinario)
                    console.log("veterinario: ", vet)
                    const dadosVet = await Veterinario.findById(vet.veterinario)
                    console.log("dados vet: ", dadosVet)

                    //apartir do animal obter o responsavel do animal
                    const user = await User.findById(eventos[i].animal.responsavel)

                    
                    //verifica se o user esta ativo
                    if(user.ativo === true){
                        console.log("user: ", user.email)

                        //enviar email a entidade competente

                        /*
                        
                        var transporter = nodemailer.createTransport({
                            service: 'Gmail',
                            auth: {
                                user: 'iseppata2018principal@gmail.com', // Your email id
                                pass: 'Andre!2018!' // Your password
                            }
                        });

                        var text = '\n\n' + 
                            '\nAlerta de Evento da plataforma PATA:\n\n' +
                            '\nEvento: ' + eventos[i].tipo +
                            '\nData: ' + eventos[i].data +
                            '\nAnimal: ' + eventos[i].animal.nome +
                            '\nVeterinário: ' + vet.name +
                            '\nEspecialidade: ' + dadosVet.especialidade +
                            '\nMorada: ' + dadosVet.morada +
                            '\nContacto: ' + dadosVet.contacto +
                            '\n\n\nCumprimentos da PATA';

                        var mailOptions = {
                            from: '<iseppata2018principal@gmail.com>', // sender address
                            //to: user.email, // list of receivers
                            to: "iseppata2018ec@gmail.com",
                            subject: 'Alerta de Evento PATA: '+ eventos[i]._id, // Subject line
                            text: text //, // plaintext body
                        };


                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                enviado = true;
                                console.log('Message sent: ' + info.response);
                            };
                            transporter.close();
                        });

                        */
                        
                    }
                }
            }
        }
        
    } catch(e) {
        console.error(`Erro: ${e.message}`);
    }
};


module.exports = {
    begin: begin
};