const nodemailer = require('nodemailer');
const config = require('../config/environment');

let smtpTransport = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    auth: {
        user: config.mail.username,
        pass: config.mail.password
    }
});

let testTransport;

function sendFakeMail(message){
    if(testTransport != null){
        return testTransport.sendMail(message);
    } else {
        return new Promise((resolve, reject) => {
            //Create a new fake account
            nodemailer.createTestAccount((err, account) => {
                if(err){
                    return reject(err);
                }
                //Create and store the fake transporter
                testTransport = nodemailer.createTransport({
                    host: 'smtp.ethereal.email',
                    port: 587,
                    secure: false, // true for 465, false for other ports
                    auth: {
                        user: account.user, // generated ethereal user
                        pass: account.pass  // generated ethereal password
                    }
                });
                //Send the message
                testTransport.sendMail(message)
                .then(response => resolve(response))
                .catch(err => reject(err));
            });
        });
        
    }
}

function sendMail(message){
    let defaultMessage = {
        from: 'no-reply@isep.ipp.pt'
    };

    message = Object.assign(defaultMessage, message);

    if(config.env == 'test'){
        return sendFakeMail(message);
    }    
    return smtpTransport.sendMail(message);
}

module.exports.sendMail = sendMail;