const Treino = require('../../models/Treino');
const Animal = require('../../models/Animal');
const User = require('../../models/User');
const Treinador = require('../../models/Treinador');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');
const TreinoRespAnimais = require('../../models/TreinoRespAnimais');

//var nodemailer = require('nodemailer');


module.exports = {

    //Get all treinos
    getTreinos: async function(req, res, next) {
        try {
            console.log("router.route('/Treinos')");
            console.log(".get(TreinosController.getTreinos)");
            console.log("req.path", req.path);
            console.log("REQ USER" , req.headers.authorization);
            console.log("REQ: ", req);
   
            treinos = await Treino.find({});

            console.log(treinos);
            res.status(200).json(treinos);
            
        }
        catch (err) {
            next(err);
        }
    },

/*
    getTreinoId2: async function(req, res, next) {
        try{
            const { treinoId } = req.params;
            const treino = await Treino.findById(treinoId);
            var newTreino = new TreinoRespAnimais();

            //validar se animal ativo
            const id_animal = treino.animal;
            animal = await Animal.findById(id_animal);
            if(animal.ativo){
                            //validar se os treinos sao realmente dos users que fazem o request
                if(req.user.role==="treinador"){
                    existe=false;
                    //const user = await User.findById(req.user._id);
                    const treinador = await Treinador.findById(req.user.treinador);

                    //percorre os treinos do treinador e verifica se o mesmo existe na sua lista
                    for(var j=0;j<treinador.treinos.length;j++){
                        if(treinador.treinos[j] == treinoId){ 
                            existe = true;

                            newTreino._id = treinos[j]._id;
                            newTreino.data = treinos[j].data;
                            newTreino.treinador = treinos[j].treinador;
                            newTreino.tipo = treinos[j].tipo;
                            newTreino.animal = treinos[j].animal;
                            newTreino.desempenho = treinos[j].desempenho;
                            newTreino.observacoes = treinos[j].observacoes;
                            newTreino.ativo = treinos[j].ativo;
    
                            const animal = await Animal.findById(treinos[j].animal);
                            const userTreinador = await User.findById(treinos[j].treinador);
    
    
                            newTreino.nomeAnimal = animal.nome;
                            newTreino.nomeTreinador = userTreinador.name;

                        }
                    }

                    if(existe){
                        res.status(200).json(newTreino);
                    }else{
                        return res.status(401).json({
                            error   : 'Treino invalido',
                            message : "Este treino não pertence ao user que fez o request."
                        });
                    }
                }else if(req.user.role === "responsavelanimais"){


                    //obter o responsavel de animais
                    const responsavelAnimais = await ResponsavelAnimais.findById(req.user.responsavelAnimais);

                    //obter o animal
                    const animalId = treino.animal;


                    //flag existe
                    existe=false;

                    //percorrer a lista de animais do responsavel dos animais e verificar se o animal fez esse treino
                    for(var i=0;i<responsavelAnimais.animais.length;i++){
                        if(responsavelAnimais.animais[i].toString() == animalId.toString()){
                            existe=true;
                        }
                    }

                    if(existe){

                        res.status(200).json(newTreino);
                    }else{
                        return res.status(401).json({
                            error   : 'Treino invalido',
                            message : "Este treino não pertence ao user que fez o request."
                        });
                    }
                }else if(req.user.role === "admin"){
                    res.status(200).json(newTreino);
                }

            }else{
                return res.status(401).json({
                    error   : 'Animal inativo',
                    message : "Este treino não é valido, o animal correspondente esta inativo."
                });
            }

     

        }catch(err){
            next(err);
        }
    },
*/


    getTreinoId: async function(req, res, next) {
        try{
            const { treinoId } = req.params;
            const treino = await Treino.findById(treinoId);
            var newTreino = new TreinoRespAnimais();

            //obter o animal
            const id_animal = treino.animal;
            animal = await Animal.findById(id_animal);

            //obter o treinador
            const id_treinador = treino.treinador;
            treinador = await User.findById(id_treinador);

            //verifica se animal ativo
            if(animal.ativo){
                
                newTreino._id = treino._id;
                newTreino.data = treino.data;
                newTreino.treinador = treino.treinador;
                newTreino.tipo = treino.tipo;
                newTreino.animal = treino.animal;
                newTreino.desempenho = treino.desempenho;
                newTreino.observacoes = treino.observacoes;
                newTreino.ativo = treino.ativo;

                newTreino.nomeAnimal = animal.nome;
                newTreino.nomeTreinador = treinador.name;



                res.status(200).json(newTreino);

            }else{
                return res.status(401).json({
                    error   : 'Animal inativo',
                    message : "Este treino não é valido, o animal correspondente esta inativo."
                });
            }

     

        }catch(err){
            next(err);
        }
    },





    //new treino
    //router.route('/Treinos')
    //.post(AnimaisController.newTreino);
    newTreino: async function(req, res, next) {
        try {
            console.log("router.route('/Treinos')");
            console.log(".post(TreinosController.newTreino)");
            const body = req.body;
            //Cria novo treino
            const treino = await new Treino(body);
            await treino.save();
            //Adiciona o treino na lista de treinos do animal
            const animalId = req.body.animal;
            const animal = await Animal.findById(animalId);

            if(animal.ativo === true){
                animal.treinos.push(treino);
                await animal.save();
                //Adiciona o treino na lista de treinos do treinador
                const treinadorId = req.body.treinador;
                const user = await User.findById(treinadorId);
                console.log("user.treinador: ", user.treinador);
                const treinador = await Treinador.findById(user.treinador);
                treinador.treinos.push(treino);
                await treinador.save();
                res.status(200).json(treino); 
            }else{
                return res.status(403).json({ error: "Tentativa de registar treino para um animal inativo" });
            }

     
        } catch (err) {
            next(err);
        }
    },


    changeTreino: async function(req, res, next) {
        try{
            //hipotese de não alterar o treinador nem o animal
            const { treinoId } = req.params;
            const treinoNovo = req.body;

            //fazer se tiver tempo...
            
            //hipotese de alterar o treinador
                //validar se veio id treinador no body
                //validar se o treinador que veio no body existe e está ativo
                //validar se o treinador que veio no body eh realmente do role treinador
            
            //hipotese de alterar o animal
                //validar se veio o id animal no body
                //validar se o animal que veio no body existe e está ativo
                

            const result = await Treino.findByIdAndUpdate(treinoId, treinoNovo);

            res.status(200).json({ success: true });

        }catch(err){
            next(err);
        }
    },








    //Delete treino
    //router.route('/Treinos/idTreino')
    //.delete(AnimaisController.delTreinoId);
    delTreinoId: async function(req, res, next) {
        try {
            console.log("router.route('/Treinos/idTreino')");
            console.log(".post(TreinosController.delTreinoId)");

            const { treinoId } = req.params;
            console.log("treinoId: ", treinoId);
            const treino = await Treino.findById(treinoId);

            if(treino.ativo)
            {
                console.log("treino: ", treino);
                treino.ativo = "false";

                const result = await treino.save();

                console.log("treino: ", treino);
                res.status(200).json(result);
            }else{
                return res.status(403).json({ error: "Tentativa de anonimizar um treino já anonimizado" });
            }

        } catch (err) {
            next(err);
        }
    },



    deleteTreinoFromDb: async function(req, res, next){
        try{
            const { treinoId } = req.params;
            const treino = await Treino.findById(treinoId);
            //elimina o treino da lista de treinos do treinador
            const usertreinador = await User.findById(treino.treinador);
            const treinador = await Treinador.findById(usertreinador.treinador).populate("treinos");


            for (var i=0; i<treinador.treinos.length; i++) {
                if (treinoId == treinador.treinos[i]) {
                    treinador.treinos.splice(i, 1);
                }
            }
            await treinador.save();
            //elimina o treino da lista de treinos do animal
            const animal = await Animal.findById(treino.animal);
            for (var i=0; i<animal.treinos.length; i++) {
                if (treinoId == animal.treinos[i]) {
                    animal.treinos.splice(i, 1);
                }
            }
            await animal.save();
            await treino.remove();
            res.status(200).json({ success: true });     
        }catch(err){
            next(err);
        }
    },


   

}

