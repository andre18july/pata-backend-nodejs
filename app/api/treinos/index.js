const router     = require('express').Router();
const controller = require('./treinos.controller');
const auth       = require('../middleware/auth.middleware');



/*

router.get('/', auth.hasRole('admin'), controller.getAnimais);
router.post('/', auth.hasRole('admin'), controller.newAnimal);

router.get('/:animalId', auth.hasRole('admin','responsavelanimais'), controller.getAnimal);
router.put('/:animalId', auth.hasRole('admin','responsavelanimais'), controller.changeAnimal);
router.delete('/:animalId', auth.hasRole('admin','responsavelanimais'), controller.deleteAnimal);
router.delete('/:animalId/fromdb', auth.hasRole('admin'), controller.deleteAnimalFromDb);
*/


router.get('/', controller.getTreinos);
router.post('/', auth.hasRole('admin', 'treinador'), controller.newTreino);


router.get('/:treinoId', auth.hasRole('admin','responsavelanimais','treinador'), controller.getTreinoId);
router.put('/:treinoId', auth.hasRole('admin','treinador'), controller.changeTreino);
router.delete('/:treinoId', auth.hasRole('admin','treinador'), controller.delTreinoId);

router.delete('/:treinoId/fromdb', auth.hasRole('admin'), controller.deleteTreinoFromDb);





module.exports = router;

