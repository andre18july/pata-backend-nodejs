const Animal = require('../../models/Animal');
const User = require('../../models/User');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');
const Treino = require('../../models/Treino');
const Evento = require('../../models/Evento');
const Ocorrencia = require('../../models/Ocorrencia');
const Intervencao = require('../../models/Intervencao')
//var nodemailer = require('nodemailer');


module.exports = {

    //Get all animais
    //router.route('/Animais')
    //.get(AnimaisController.getAnimais);
    getAnimais: async function(req, res, next) {
        try {
            console.log("router.route('/Animais')");
            console.log(".get(AnimaisController.getAnimais)");
            //console.log("req.path", req.path);

            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);
   
            animais = await Animal.find({});

            //console.log(animais);
            res.status(200).json(animais);
            
        }
        catch (err) {
            next(err);
        }
    },


    //Get all animais ativos
    //router.route('/Animais/ativos')
    //.get(AnimaisController.getAnimaisAtivos);
    getAnimaisAtivos: async function(req, res, next) {
        try {
            console.log("router.route('/Animais/ativos')");
            console.log(".get(AnimaisController.getAnimaisAtivos)");
            console.log("req.path", req.path);

            console.log("REQ USER" , req.headers.authorization);
            console.log("REQ: ", req);
   
            animais = await Animal.find({});

            var newAnimais = new Array();


            for(var i=0;i<animais.length;i++){
                if(animais[i].ativo===true){
                    newAnimais.push(animais[i]);
                }
            }

            console.log(newAnimais);
            res.status(200).json(newAnimais);
            
        }
        catch (err) {
            next(err);
        }
    },



    //new animal
    //router.route('/Animais')
    //.post(AnimaisController.newAnimal);
    newAnimal: async function(req, res, next) {
        try {
            console.log("router.route('/Animais')");
            console.log(".post(AnimaisController.newAnimal)");

            var body = req.body;

                        
            if(body.fotoAnimal){
                if(body.fotoAnimal.changingThisBreaksApplicationSecurity === ""){

                    body.fotoAnimal=null;
                }
            }

            //encontra o responsavel de animais atual
            const user_respAnimais = await User.findById(body.responsavel);
            console.log("user_respAnimais: ", user_respAnimais);
            const respAnimais = await ResponsavelAnimais.findById(user_respAnimais.responsavelAnimais);
            console.log("respAnimais: ", respAnimais);

            //criar um novo animal
            const newAnimal = body;
            const animal = new Animal(newAnimal);
            await animal.save();

            // adicionar o novo animal ao responsavel de animais
            respAnimais.animais.push(animal);
            await respAnimais.save();


            res.status(200).json(animal);


        } catch (err) {
            next(err);
        }
    },


    getAnimal: async function(req, res, next) {
        try{
            const { animalId } = req.params;
            const animal = await Animal.findById(animalId);

            console.log("animal: ", animal);
            res.status(200).json(animal);

        }catch(err){
            next(err);
        }
    },

/*
    changeAnimal: async function(req, res, next) {
        try{
            const { animalId } = req.params;
            const animal2 = req.body;

            if(animal2.responsavel){
          
                const animal1 = await Animal.findById(animalId);


                if(animal1.responsavel == animal2.responsavel){

                    const result = await Animal.findByIdAndUpdate(animalId, animal2);
                }else{
                    return res.status(403).json({ error: "Tentativa de atribuir animal a outro responsavel impedida" });
                }
            }else{
                const result = await Animal.findByIdAndUpdate(animalId, animal2);
            }
            
            res.status(200).json({ success: true });

        }catch(err){
            next(err);
        }
    },
*/

    changeAnimal: async function(req, res, next) {
        try{
            const { animalId } = req.params;

            var body = req.body;

            if(body.fotoAnimal){
                if(body.fotoAnimal.changingThisBreaksApplicationSecurity === ""){

                    body.fotoAnimal=null;
                }
            }


            const animal2 = body;

            //validar se veio o id do responsavel no body, se é necessário alterar o responsavel

            
            if(animal2.responsavel && req.user.role === "admin"){

                responsavel = await User.findById(animal2.responsavel);

                if(responsavel){
                    if(responsavel.ativo && responsavel.role === "responsavelanimais"){

                
          
                        const animal1 = await Animal.findById(animalId);
    
    
                        if(animal1.responsavel == animal2.responsavel){
    
                            const result = await Animal.findByIdAndUpdate(animalId, animal2);
                        }else{
                            console.log("EEEE");
                            
                            //return res.status(403).json({ error: "Tentativa de atribuir animal a outro responsavel impedida" });
    
                            //atualiza o animal 1 com os dados do animal 2
                            const result = await Animal.findByIdAndUpdate(animalId, animal2);
                            if(result){
    
                                const animalAtualizado = await Animal.findById(animalId);
    
                                //1- obter os dois responsaveis
                                const user_resp1 = await User.findById(animal1.responsavel);
                                const resp1 = await ResponsavelAnimais.findById(user_resp1.responsavelAnimais);
                                const user_resp2 = await User.findById(animal2.responsavel);
                                const resp2 = await ResponsavelAnimais.findById(user_resp2.responsavelAnimais);
    
                                //2- Adicionar o animal no responsavel 2
                                resp2.animais.push(animalAtualizado);
    
                                //3 - Eliminar o animal no responsavel 1
    
                                console.log("resp1 lenght: ", resp1.animais.length);
                                console.log("resp1 animais: ", resp1.animais);
    
                                for (var i=0; i<resp1.animais.length; i++) {
                                    if (animalId == resp1.animais[i]) {
                                        resp1.animais.splice(i, 1);
                                    }
                                }
    
                                const result2 = await resp1.save();
                                const result3 = await resp2.save();
                            }
                        }
                    }else{
                        return res.status(403).json({ error: "Tentativa de atribuir animal a user invalido impedida" });
                    }
                }else{
                    return res.status(403).json({ error: "User invalido" });
                }


                
                
            }else{
                const result = await Animal.findByIdAndUpdate(animalId, animal2);
            }
            
            res.status(200).json({ success: true });

        }catch(err){
            next(err);
        }
    },

    

    deleteAnimal: async function(req, res, next){
        try{
            const { animalId } = req.params;
            console.log("animalId: ", animalId);
            const animal = await Animal.findById(animalId);
            

            if(animal.ativo)
            {

                animal.ativo = "false";
                const result = await animal.save();

                //desativar todos os treinos relacionados com o animal
                var treinos = await Treino.find({});
                
                
                for(var i=0;i<treinos.length;i++){
                    if(treinos[i].animal.toString() === animalId.toString()){
                        treinos[i].ativo = false;
                        await treinos[i].save();
                        //console.log("dentro treinos")
                    }
                }


                //desativar todos os eventos relacionados com o animal
                var eventos = await Evento.find({});
                var intervencoes = await Intervencao.find({});
                for(var i=0;i<eventos.length;i++){
                    if(eventos[i].animal.toString() === animalId.toString()){
                        eventos[i].ativo = false;
                        await eventos[i].save();
                        //console.log("dentro eventos")
                        for(var j=0;j<intervencoes.length;j++){
                            if(eventos[i]._id.toString() === intervencoes[j].evento.toString()){
                                //console.log("dentro intervencoes");
                                intervencoes[j].ativo=false;
                                await intervencoes[j].save();
                                //console.log("dentro int")
                            }
    
                        }
                    }
                }


                //desativar todas as ocorrencias relacionados com o animal
                var ocorrencias = await Ocorrencia.find({});
                for(var i=0;i<ocorrencias.length;i++){
                    if(ocorrencias[i].animal != null){
                        if(ocorrencias[i].animal.toString() === animalId.toString()){
                            ocorrencias[i].ativo = false;
                            await ocorrencias[i].save();
                            //console.log("dentro ocorrencias")
                        }
                    }
                }

                res.status(200).json("success");

                
            }else{
                return res.status(403).json({ error: "Tentativa de anonimizar um animal já anonimizado" });
            }

            
        }catch(err){
            next(err);
        }
    },


    deleteAnimalFromDb: async function(req, res, next){
        try{
            const { animalId } = req.params;
            const animal = await Animal.findById(animalId);
            const userrespAnimal = await User.findById(animal.responsavel).populate("responsavelAnimais");

            console.log("responsavel do animal hehe: ", userrespAnimal);

            const respAnimal = userrespAnimal.responsavelAnimais;

            //elimina o animal da lista de animais do user
            for (var i=0; i<respAnimal.animais.length; i++) {
                if (animalId == respAnimal.animais[i]) {
                    respAnimal.animais.splice(i, 1);
                }
            }


            //desativar todos os treinos relacionados com o animal
            var treinos = await Treino.find({});
            for(var i=0;i<treinos.length;i++){
                if(treinos[i].animal.toString() === animalId.toString()){
                    console.log("dentro treinos");
                    await treinos[i].remove();
                }
            }


            //desativar todos os eventos relacionados com o animal
            var eventos = await Evento.find({});
            var intervencoes = await Intervencao.find({});
            for(var i=0;i<eventos.length;i++){
                if(eventos[i].animal.toString() === animalId.toString()){
                    console.log("dentro eventos");
                    await eventos[i].remove();
                    //obter intervencoes do evento
                    for(var j=0;j<intervencoes.length;j++){
                        if(eventos[i]._id.toString() === intervencoes[j].evento.toString()){
                            console.log("dentro intervencoes");
                            intervencoes[j].remove();
                        }

                    }

                }
            }


            //desativar todas as ocorrencias relacionados com o animal
            var ocorrencias = await Ocorrencia.find({});
            for(var i=0;i<ocorrencias.length;i++){
                if(ocorrencias[i].animal != null){
                    console.log("ocorrencias[i].animal: ", ocorrencias[i].animal.toString());
                    console.log("animalId: ", animalId);
                    if(ocorrencias[i].animal.toString() === animalId.toString()){
                        console.log("dentro ocorrencias");
                        await ocorrencias[i].remove();
                    }
                }
            }

            await respAnimal.save();
            await animal.remove();

            //console.log("animal: ", animal);
            res.status(200).json({ success: true });
            

        }catch(err){
            next(err);
        }
    }

    

}




/*
const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');
const moment = require('moment');

async function getList()
{
    var animais = await gdm.get('/api/animais');
    return animais;
}

function list(req, res){
    getList()
    .then(response => 
        res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

function listComments(req, res){
    gdm.get(`/api/animais/${req.params.id}/comentarios`)
    .then(response => {
        //Transform the dates from DD/MM/YYYY to YYYY/MM/DD to follow the standard in the other controllers
        const data = response.data.map(c => {
            c.data = moment(c.data, 'DD/MM/YYYY').format('YYYY/MM/DD');
            return c;
        });
        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function createComment(req, res){
    req.body.data = req.body.data || moment().format('DD/MM/YYYY');
    req.body.apresentacaoId = req.params.id;

    gdm.post(`/api/animais/${req.params.id}/comentarios`, req.body)
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list          : list,
    listComments  : listComments,
    createComment : createComment,
    getList : getList
};
*/

