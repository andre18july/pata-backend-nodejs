const router     = require('express').Router();
const controller = require('./animais.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', auth.hasRole('admin'), controller.getAnimais);
router.get('/ativos', auth.hasRole('admin'), controller.getAnimaisAtivos);
router.post('/', auth.hasRole('admin'), controller.newAnimal);

router.get('/:animalId', auth.hasRole('admin','responsavelanimais','veterinario', 'treinador'), controller.getAnimal);
router.put('/:animalId', auth.hasRole('admin','responsavelanimais'), controller.changeAnimal);
router.delete('/:animalId', auth.hasRole('admin','responsavelanimais'), controller.deleteAnimal);
router.delete('/:animalId/fromdb', auth.hasRole('admin'), controller.deleteAnimalFromDb);

module.exports = router;

