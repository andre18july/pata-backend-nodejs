const router     = require('express').Router();
const controller = require('./eventos.controller');
const auth       = require('../middleware/auth.middleware');



router.get('/', controller.getEventos);
router.post('/', auth.hasRole('admin', 'veterinario'), controller.newEvento);


router.get('/:eventoId', auth.hasRole('admin','responsavelanimais','veterinario'), controller.getEventoId);
router.put('/:eventoId', auth.hasRole('admin','veterinario'), controller.changeEvento);
router.delete('/:eventoId', auth.hasRole('admin','veterinario'), controller.delEventoId);

router.delete('/:eventoId/fromdb', auth.hasRole('admin'), controller.deleteEventoFromDb);





module.exports = router;

