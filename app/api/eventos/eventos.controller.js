const Evento = require('../../models/Evento');
const Intervencao = require('../../models/Intervencao');
const Animal = require('../../models/Animal');
const User = require('../../models/User');
const Veterinario = require('../../models/Veterinario');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');
const EventoRespAnimais = require('../../models/EventoRespAnimais');

module.exports = {

    //Get all eventos
    getEventos: async function(req, res, next) {
        try {
            console.log("router.route('/Eventos')");
            console.log(".get(EventosController.getEventos)");
            //console.log("req.path", req.path);
            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);
   
            eventos = await Evento.find({});

            console.log(eventos);
            res.status(200).json(eventos);
            
        }
        catch (err) {
            next(err);
        }
    },


    getEventoId: async function(req, res, next) {
        try{
            const { eventoId } = req.params;
            console.log("Evento ID: ",eventoId);
            const evento = await Evento.findById(eventoId);
            console.log("Evento: ", evento);

            //validar se animal ativo
            const id_animal = evento.animal;


            newEvento = new EventoRespAnimais();
            newEvento._id = evento._id;
            newEvento.data = evento.data;
            newEvento.veterinario = evento.veterinario;
            newEvento.animal = evento.animal;
            newEvento.observacoes = evento.observacoes;
            newEvento.tipo = evento.tipo;
            newEvento.ativo = evento.ativo;

            const eventoAnimal = await Animal.findById(evento.animal);
            const eventoVeterinario = await User.findById(evento.veterinario);


            newEvento.nomeAnimal = eventoAnimal.nome;
            newEvento.nomeVeterinario = eventoVeterinario.name;

            animal = await Animal.findById(id_animal);
            if(animal.ativo){

                res.status(200).json(this.newEvento);

            }else{
                return res.status(401).json({
                    error   : 'Animal inativo',
                    message : "Este evento não é valido, o animal correspondente esta inativo."
                });
            }

        }catch(err){
            next(err);
        }
    },



    //new evento
    //router.route('/Eventos')
    //.post(EventosController.newEvento);
    newEvento: async function(req, res, next) {
        try {
            console.log("router.route('/Eventos')");
            console.log(".post(EventosController.newEvento)");
            const body = req.body;

            //obter animal
            const animal = await Animal.findById(body.animal);
            

            if(animal.ativo === true){
                //Cria novo evento
                const evento = await new Evento(body);
                await evento.save();

                res.status(200).json(evento); 
            }else{
                return res.status(403).json({ error: "Tentativa de registar treino para um animal inativo" });
            }

        } catch (err) {
            next(err);
        }
    },


    changeEvento: async function(req, res, next) {
        try{

            const { eventoId } = req.params;
            const eventoNovo = req.body;

            const result = await Evento.findByIdAndUpdate(eventoId, eventoNovo);

            res.status(200).json({ success: true });

        }catch(err){
            next(err);
        }
    },



    //Delete Evento
    //router.route('/Eventos/idEvento')
    //.delete(EventosController.delEventoId);
    delEventoId: async function(req, res, next) {
        try {
            console.log("router.route('/Eventos/idEvento')");
            console.log(".post(EventosController.delEventoId)");

            const { eventoId } = req.params;
            const evento = await Evento.findById(eventoId);

            if(evento.ativo)
            {
                console.log("evento: ", evento);
                evento.ativo = false;

                await evento.save();

                //anonimizar as intervencoes relacionadas com este evento
                const intervencoes = await Intervencao.find({});
                var newIntervencoes = new Array();
    
                for(var i=0;i<intervencoes.length;i++){
                    if(intervencoes[i].evento.toString() === eventoId.toString()){
                        intervencoes[i].ativo = false;

                    }
                    await intervencoes[i].save();
                }

                res.status(200).json({ success: true });
            }else{
                return res.status(403).json({ error: "Tentativa de anonimizar um evento já anonimizado" });
            }

        } catch (err) {
            next(err);
        }
    },



    deleteEventoFromDb: async function(req, res, next){
        try{
            const { eventoId } = req.params;
            const evento = await Evento.findById(eventoId);

            //apagar as intervencoes relacionadas com o evento

            const intervencoes = await Intervencao.find({});
            var newIntervencoes = new Array();

            for(var i=0;i<intervencoes.length;i++){
                if(intervencoes[i].evento.toString() === eventoId.toString()){
                    await intervencoes[i].remove();
                }
            }

            await evento.remove();
            res.status(200).json({ success: true });     
        }catch(err){
            next(err);
        }
    },


   

}

