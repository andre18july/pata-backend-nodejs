const router     = require('express').Router();
const controller = require('./auth.controller');

router.post('/login', controller.login);
router.post('/register', controller.register);
router.post('/registerTreinador', controller.registerTreinador);
router.post('/registerVeterinario', controller.registerVeterinario);
router.post('/registerResponsavelAnimais', controller.registerResponsavelAnimais);



module.exports = router;