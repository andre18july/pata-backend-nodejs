const User = require('../../models/User');
const Treinador = require('../../models/Treinador');
const Veterinario = require('../../models/Veterinario');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');
const authService = require('./auth.service');
const utils = require('../../components/utils');
const _ = require('lodash');
const Log2 = require('../../components/logs/log');


function login(req, res) {
    User.findOne({
            email: req.body.email
        })
        .select('+password')
        .exec()
        .then(user => {
            if (!user) {
                return res.status(401).json({ error: 'invalid_user', 
                message: 'Utilizador inválido.' });
            }
            if (!user.checkPassword(req.body.password)) {
                return res.status(401).json({ error: 'invalid_user', 
                message: "O email e/ou password que introduziu não são válidos." });
            }
            if (!user.ativo){
                return res.status(401).json({ error: 'invalid_user', 
                message: "O utilizador carece de validação do administrator." });
            } 
            
            let token = authService.signToken(user);
            return res.status(200).json({ user: user, token: token });
        })
        .catch(utils.handleError);
}

function register(req, res) {
    console.log("req.body: ", req.body);
    let user = new User();
    user.email = req.body.email;
    user.name = req.body.name;
    user.password = req.body.password;
    user.role = req.body.role;


    if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email)){

        return res.status(403).json({ error: "O email introduzido não é válido" });
    }


    user.save()
        .then(user => {
            Log2.logger.info("User " + user.name + " do tipo " + user.role + " criado!");
            // create a token
            let token = authService.signToken(user);
            let data = {
                _id: user._id,
                email: user.email,
                name: user.name,
                role: user.role,
                ativo: user.ativo,
                token: token.token,
                expirationDate: token.expirationDate
            };
            res.status(201).json(data);
        })
        .catch(utils.handleError(req, res));
}

// Register feito com promises
const registerResponsavelAnimais = async(req, res, next) => {
    try{

        console.log("req.body: ", req.body);

        let responsavelAnimais = new ResponsavelAnimais();

        let user = new User();

        user.email = req.body.email;
        user.name = req.body.name;
        user.password = req.body.password;
        user.responsavelAnimais = responsavelAnimais._id;
        user.role = req.body.role;

        if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email)){

            return res.status(403).json({ error: "O email introduzido não é válido" });
        }

        
        user.save()
        .then(user => {
            Log2.logger.info("User " + user.name + " do tipo " + user.role + " criado!");

            responsavelAnimais.user = user._id;

    
            responsavelAnimais.save()
            .then(responsavelAnimais => {
                //Log2.logger.info("User RespAnimais " + responsavelAnimais.name + " do tipo " + responsavelAnimais.role + " criado!");
                // create a token
                let token = authService.signToken(user);
    
                let data = {
                    _id: responsavelAnimais._id,
                    user: user,
                    token: token.token,
                    expirationDate: token.expirationDate
                };
    
                res.status(201).json(data);
            })
            .catch(utils.handleError(req, res));
        })
        .catch(utils.handleError(req, res));



    } catch(err) {
        next(err);
    }
};


//registar treinador com promises
const registerTreinador = async(req, res, next) => {
    try{

        console.log("req.body: ", req.body);

        let treinador = new Treinador();

        let user = new User();

        user.email = req.body.email;

        user.name = req.body.name;
        user.password = req.body.password;
        user.role = req.body.role;
        user.treinador = treinador._id;
        user.ativo = false;

        if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email)){

            return res.status(403).json({ error: "O email introduzido não é válido" });
        }

        
        user.save()
        .then(user => {
            Log2.logger.info("User " + user.name + " do tipo " + user.role + " criado!");

            treinador.user = user._id;

            treinador.credencial = req.body.credencial;
            treinador.morada = req.body.morada;
            treinador.contacto = req.body.contacto;
            
            console.log("treinador: ", treinador);
    
            treinador.save()
            .then(treinador => {
                //Log2.logger.info("User Treinador " + treinador.name + " do tipo " + treinador.role + " criado!");
                // create a token
                let token = authService.signToken(user);
    
                let data = {
                    _id: treinador._id,
                    user: user,
                    credencial: treinador.credencial,
                    morada: treinador.morada,
                    contacto: treinador.contacto,
                    token: token.token,
                    expirationDate: token.expirationDate
                };
    
                res.status(201).json(data);
            })
            .catch(utils.handleError(req, res));
        })
        .catch(utils.handleError(req, res));



    } catch(err) {
        next(err);
    }
};




//registar veterinario com promises
const registerVeterinario = async(req, res, next) => {
    try{

        console.log("req.body: ", req.body);

        let veterinario = new Veterinario();

        let user = new User();

        user.email = req.body.email;

        user.name = req.body.name;
        user.password = req.body.password;
        user.role = req.body.role;
        user.veterinario = veterinario._id;
        user.ativo = false;

        if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email)){

            return res.status(403).json({ error: "O email introduzido não é válido" });
        }

        
        user.save()
        .then(user => {
            Log2.logger.info("User " + user.name + " do tipo " + user.role + " criado!");

            veterinario.user = user._id;
            veterinario.credencial = req.body.credencial;
            veterinario.especialidade = req.body.especialidade;
            veterinario.horario = req.body.horario;
            veterinario.morada = req.body.morada;
            veterinario.contacto = req.body.contacto;
            
            console.log("veterinario: ", veterinario);
    
            veterinario.save()
            .then(veterinario => {
                //Log2.logger.info("User veterinario " + veterinario.name + " do tipo " + veterinario.role + " criado!");
                // create a token
                let token = authService.signToken(user);
    
                let data = {
                    _id: veterinario._id,
                    user: user,
                    credencial: veterinario.credencial,
                    especialidade: veterinario.especialidade,
                    horario: veterinario.horario,
                    morada: veterinario.morada,
                    contacto: veterinario.contacto,
                    token: token.token,
                    expirationDate: token.expirationDate
                };
    
                res.status(201).json(data);
            })
            .catch(utils.handleError(req, res));
        })
        .catch(utils.handleError(req, res));



    } catch(err) {
        next(err);
    }
};





module.exports = {
    login: login,
    register: register,
    registerTreinador: registerTreinador,
    registerResponsavelAnimais: registerResponsavelAnimais,
    registerVeterinario: registerVeterinario
};