const router     = require('express').Router();
const controller = require('./ocorrencias.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', controller.getOcorrencias);
router.get('/:ocorrenciaId', controller.getOcorrenciaId);
router.post('/',controller.newOcorrencia);


module.exports = router;

