const Animal = require('../../models/Animal');
const Ocorrencia = require('../../models/Ocorrencia');
const User = require('../../models/User');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');
const nodemailer = require('nodemailer');



module.exports = {

    //Get all ocorrencias
    //router.route('/Ocorrencias')
    //.get(OcorrenciasController.getOcorrencias);
    getOcorrencias: async function(req, res, next) {
        try {
            console.log("router.route('/Ocorrencias')");
            console.log(".get(OcorrenciasController.getOcorrencias)");
            //console.log("req.path", req.path);

            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);
   
            ocorrencias = await Ocorrencia.find({});

            //console.log(ocorrencias);

            res.status(200).json(ocorrencias);
            
        }
        catch (err) {
            next(err);
        }
    },



    //Get uma ocorrencia
    //router.route('/Ocorrencias/ocorrenciaId')
    //.get(OcorrenciasController.getOcorrenciaId);
    getOcorrenciaId: async function(req, res, next) {
        try{
            console.log("router.route('/Ocorrencias/ocorrenciaId')");
            console.log(".post(OcorrenciasController.getOcorrenciaId)");
            
            const { ocorrenciaId } = req.params;
            const ocorrencia = await Ocorrencia.findById(ocorrenciaId);

            //validar se ocorrencia esta ativa
            if(ocorrencia.ativo){

                res.status(200).json(ocorrencia);
            }else{
                return res.status(401).json({
                    error   : 'Ocorrencia inativa',
                    message : "Esta ocorrência não se encontra num estado ativa."
                });
            }

        }catch(err){
            next(err);
        }
    },



    //Post nova ocorrencia
    //router.route('/Ocorrencias')
    //.post(OcorrenciasController.newOcorrencia);
    newOcorrencia: async function(req, res, next) {
        try {
            console.log("router.route('/Ocorrencias')");
            console.log(".post(OcorrenciasController.newOcorrencia)");

   
            const body = req.body;
            console.log("Body: ", body);
            

            if(body.animal===""){
                body.animal=null;
            }

            if(body.fotoAnimal){
                if(body.fotoAnimal.changingThisBreaksApplicationSecurity === ""){

                    body.fotoAnimal=null;
                }
            }

            body.responsavelAnimais=null;

            const ocorrencia = await new Ocorrencia(body);


            //valida se o animal é de algum responsavel dos animais em sistema
            if(body.animal){
                console.log("O animal tem dono no sistema");
                //obter o animal da BD
                const animal = await Animal.findById(body.animal);
                if(animal.ativo){
                    ocorrencia.tipoAnimal = animal.tipo;
                    ocorrencia.racaAnimal = animal.raca;
                    //obter o responsavel de animais na BD
                    const respAnimais = await User.findById(animal.responsavel);
                    //ocorrencia.contacto = respAnimais.email;
                }else{
                    return res.status(401).json({
                        error   : 'Animal inativo',
                        message : "O animal que associou a esta ocorrencia esta inativo no sistema."
                    });
                }
            
            }else{
                console.log("O animal nao tem dono no sistema");
            }

            //no caso de acidente
            if(body.tipo === "acidente"){
                if(!body.estadoAnimal){
                    return res.status(401).json({
                        error   : 'Estado tem de estar preenchido',
                        message : "No caso de ocorrencia de acidente o estado tem de estar preenchido."
                    });
                }  
            }

            //no caso de maus tratos
            if(body.tipo === "maustratos"){
                if(!body.agressor){
                    return res.status(401).json({
                        error   : 'Agressor do animal tem de estar preenchido',
                        message : "No caso de ocorrencia de maus tratos o agressor tem de estar preenchido."
                    });
                }
            }

            //geral
            if(!body.descricao){
                return res.status(401).json({
                    error   : 'Descricao da ocorrencia tem de estar preenchido',
                    message : "Descricao da ocorrencia tem de estar preenchido."
                });
            }
            if(!body.tipoAnimal && !ocorrencia.tipoAnimal){
                return res.status(401).json({
                    error   : 'Tipo de animal tem de estar preenchido',
                    message : "Tipo de animal tem de estar preenchido"
                });
            }


            

            //enviar email a entidade competente
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'iseppata2018principal@gmail.com', // Your email id
                    pass: 'Andre!2018!' // Your password
                }
            });

            var text =  '\nOcorrência PATA:\n\n' + '\nID: ' + ocorrencia._id + '\nTipo: ' + ocorrencia.tipo +
            '\nDescrição: ' + ocorrencia.descricao + '\nId Animal: ' + ocorrencia.animal +
            '\nTipo do animal: ' + ocorrencia.tipoAnimal + '\nRaça do animal: ' + ocorrencia.racaAnimal +
            '\nEstado do animal: ' + ocorrencia.estadoAnimal + '\nLocalizacao: ' + ocorrencia.localizacao +
            '\nLatitude: ' + ocorrencia.lat +'\nLongitude: ' + ocorrencia.long +
            '\nContacto do requerente: ' + ocorrencia.contacto +'\nDescrição do agressor: ' + ocorrencia.agressor +
            '\n\n\nCumprimentos da PATA';

            var mailOptions = {
                from: '<iseppata2018principal@gmail.com>', // sender address
                to: "iseppata2018ec@gmail.com", // list of receivers
                subject: 'Ocorrência PATA do tipo ' + body.tipo +  ' - ID: ' + ocorrencia._id, // Subject line
                text: text //, // plaintext body
            };

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error); res.json({ error });
                } else {
                    console.log('Message sent: ' + info.response);
                    res.json({ yo: info.response });
                };
                transporter.close();
            });
            

            

            ocorrencia.notificada=true;
            const result = await ocorrencia.save();
            res.status(200).json(ocorrencia);

            
            

        }
        catch (err) {
            next(err);
        }
    }, 


}