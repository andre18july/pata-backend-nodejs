const utils = require('../../components/utils');
const User = require('../../models/User');
const UserCompleto = require('../../models/UserCompleto');
const Animal = require('../../models/Animal');
const Treinador = require('../../models/Treinador');
const Veterinario = require('../../models/Veterinario');
const nodemailer = require('nodemailer');




function list(req, res){
    
    console.log("dentro de utilizadores list")
    let query = {name : {$ne: 'anonymous'}};

    console.log("query: ", query);
    console.log("query.role: ", query.role);

    if(req.query.role){
        query.role = req.query.role;
    }
    User.find(query)
    .then(
        users => res.status(200).json(users))
    .catch(utils.handleError(req, res));
}


//lista todos os vets e treinadores que fizeram pre registo e aguardam validação
const listUsersPreRegisto = async(req,res,next) => {
    try{
        console.log("dentro de listUsersPreRegisto")
        //console.log("REQ: ", req.user);
        const user = req.user;
        
        //valida se e admin
        if(user.role==='admin'){
            const users = await User.find({});
            
            var newUsers = new Array();

            for(var i=0;i<users.length; i++){
                //verifica se user é vet ou treinador e se tem estado ativo a false
                if((users[i].role==="treinador" || users[i].role==="veterinario") & users[i].ativo===false){
                    if(users[i].role==="treinador"){
                        const treinadores = await Treinador.find({}).lean();
                        for(j=0;j<treinadores.length;j++){
                            //console.log("treinadores[j]: ", treinadores[j].user);
                            //console.log("users[i]: ", users[i]._id);
                            if(treinadores[j].user.toString()===users[i]._id.toString()){
                                
                                var newUser = new UserCompleto();                               
                                newUser.userid = users[i]._id;
                                newUser.name = users[i].name;
                                newUser.email = users[i].email;
                                newUser.role = users[i].role;
                                newUser.ativo = users[i].ativo;
                                newUser.credencial = treinadores[j].credencial;
                                newUser.morada = treinadores[j].morada;
                                newUser.contacto = treinadores[j].contacto;

                                newUsers.push(newUser);
          
                                //console.log("user com cred: " , newUser);
                            }
                        }  
                    }else if(users[i].role==="veterinario"){
                  
                        
                        const veterinarios = await Veterinario.find({}).lean();
                        for(j=0;j<veterinarios.length;j++){
                            //console.log("treinadores[j]: ", treinadores[j].user);
                            //console.log("users[i]: ", users[i]._id);
                            if(veterinarios[j].user.toString()===users[i]._id.toString()){
                                
                                var newUser = new UserCompleto();                               
                                newUser.userid = users[i]._id;
                                newUser.name = users[i].name;
                                newUser.email = users[i].email;
                                newUser.role = users[i].role;
                                newUser.ativo = users[i].ativo;
                                newUser.credencial = veterinarios[j].credencial;
                                newUser.morada = veterinarios[j].morada;
                                newUser.contacto = veterinarios[j].contacto;
                                newUser.especialidade = veterinarios[j].especialidade;
                                newUser.horario = veterinarios[j].horario;

                                newUsers.push(newUser);
            
                                console.log("user com cred: " , newUser);
                            }
                        }  
                        
                      
                    }
                    

                }
            }

            res.status(200).json(newUsers)

        }else{
            return res.status(403).json({ error: "Utilizador inválido" });
        }
        


    }catch(err){
        next(err);
    }

};




const changeUser = async(req,res,next) => {
    try{
        var userChanged = false;

        //obter o userid nos parametros
        const { userId } = req.params;
        console.log("User Id: ", userId);
        //obter o user atraves do userid
        const user = await User.findById(userId);
        //alterar o nome do user
        if(req.body.name){
            user.name = req.body.name;
            userChanged = true;
            console.log("req.body.nome: ", req.body.name);
        }
        
        //alterar o email do user
        if(req.body.email){
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email))
            {
                user.email = req.body.email;
                userChanged = true;
                console.log("req.body.email: ", req.body.email);
            }else{
                return res.status(403).json({ error: "O email introduzido não é válido" });
            }
        }

        //gravar user
        if(userChanged){
            const result = await user.save();
            console.log("Grava o user");
            res.status(200).json(result)
        }

        res.status(200).json({result: "Nao houveram alterações"});


    }catch(err){
        next(err);
    }
};


const changeUserEstado = async(req,res,next) => {
    try{

        console.log("Altera o estado: ")
        //obter o userid nos parametros
        const { userId } = req.params;
        console.log("User Id: ", userId);
        //obter o user atraves do userid
        const user = await User.findById(userId);
        //alterar o nome do user

        user.ativo = true;

        const result = await user.save();
        console.log("Grava o user");
        res.status(200).json(result)



    }catch(err){
        next(err);
    }
};



const rejeitauser = async(req,res,next) => {
    try{

        console.log("Reijeita user")
        //obter o userid nos parametros
        const { userId } = req.params;
        console.log("User Id: ", userId);
        //obter o user atraves do userid
        const user = await User.findById(userId);

        var enviado = false;

        //envia email ao utilizador a pedir credencial
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'iseppata2018principal@gmail.com', // Your email id
                pass: 'Andre!2018!' // Your password
            }
        });

        
        var text =  
        '\nA sua credêncial é inválida.' +
        '\nPor favor envie-nos a sua credêncial correcta para que possamos validar o seu utilizador.' + 
        '\n\n\nCumprimentos da PATA';

        var mailOptions = {
            from: '<iseppata2018principal@gmail.com>', // sender address
            to: user.email, // list of receivers
            subject: 'Utilizador inválido', // Subject line
            text: text //, // plaintext body
        };


        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                res.json({ error });
            } else {
                enviado = true;
                console.log('Message sent: ' + info.response);
                res.json({ yo: info.response });
            };
            transporter.close();
        });

        if (enviado == true) {
            res.status(200).json("Email enviado");
        }


    }catch(err){
        next(err);
    }
};


const changeUserPassword = async(req,res,next) => {
    try{
        console.log("changeUserPassword");
        const { userId } = req.params;
        //console.log("changePassword userId: ", userId);

        //obter password do body
        const password = req.body.password;
        //console.log("password: ", password);

        //obter o user na bd
        const user = await User.findById(userId);
        //console.log("changePassword user: ", user);

        user.password = password;

        //console.log("User depois da pass: ", user);

        const result = await user.save();

        res.status(200).json(result);

    }catch(err){
        next(err);
    }
};



const listId = async(req,res,next) => {
    try{
        console.log("dentro de utilizadores listId")
        const { userId } = req.params;
        console.log("userId: ", userId);
        const user = await User.findById(userId);


        if(user.role === 'treinador' || user.role === "veterinario"){
            var newUser = new UserCompleto();

            if(user.role === "treinador"){
                const treinador = await Treinador.findById(user.treinador);
                newUser.credencial = treinador.credencial;
                console.log("TRN: ", treinador)
            }else if(user.role === "veterinario"){
                const veterinario = await Veterinario.findById(user.veterinario);
                newUser.credencial = veterinario.credencial;
                console.log("Vet: ", veterinario)
            }



            newUser.userid = user._id;
            newUser.name = user.name;
            newUser.email = user.email;
            newUser.role = user.role;
            newUser.ativo = user.ativo;
            res.status(200).json(newUser);
        }

        res.status(200).json(user);


    }catch(err){
        next(err);
    }
};




module.exports = {
    list : list,
    changeUser,
    changeUserPassword,
    listId,
    listUsersPreRegisto,
    changeUserEstado,
    rejeitauser

};