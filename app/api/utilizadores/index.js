const router     = require('express').Router();
const controller = require('./utilizadores.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/',auth.hasRole('admin'), controller.list);
router.get('/listpreregisto',auth.hasRole('admin'), controller.listUsersPreRegisto);
router.get('/:userId', controller.listId);
router.put('/:userId', controller.changeUser);
router.put('/:userId/alteraestado', auth.hasRole('admin'), controller.changeUserEstado);
router.put('/:userId/rejeitauser/rejeita', auth.hasRole('admin'), controller.rejeitauser);
router.put('/:userId/password', auth.hasRole('responsavelanimais','treinador','veterinario'), controller.changeUserPassword);

module.exports = router;