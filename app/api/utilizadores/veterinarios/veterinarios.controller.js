const utils = require('../../../components/utils');
const User = require('../../../models/User');
const Animal = require('../../../models/Animal');
const Evento = require('../../../models/Evento');
const Treinador = require('../../../models/Treinador');

function list(req, res){
    let query = {name : {$ne: 'anonymous'}};

    if(req.query.role == "treinador"){
        query.role = req.query.role;
    }
    User.find(query)
    .then(
        users => res.status(200).json(users))
    .catch(utils.handleError(req, res));
}


//devolve os animais todos da clinica de veterinaria
const getVeterinarioAnimais = async(req, res, next) => {
    try{


        //obter o id do veterinario
        const { userId } = req.params;

        //obter o veterinario
        const user_veterinario = await User.findById(userId);

        //Verifica se o user é de facto veterinario
        if(user_veterinario.role==='veterinario'){

            var newAnimais = new Array();
            var newEventos = new Array();

            //obter todas os eventos
            const eventos = await Evento.find({}).populate("animal");

            //obter só os eventos do userid
            for(var i=0;i<eventos.length;i++){
                if(eventos[i].veterinario.toString()===userId.toString()){
                    const animal = await Animal.findById(eventos[i].animal).populate("responsavel");
                    if (animal.ativo === true) {         
                        haveAnimal = false;
                        for(var j=0;j < newAnimais.length;j++){
                             if(animal.id == newAnimais[j].id){
                                 haveAnimal = true;
                            }
                        }
                         if(!haveAnimal){
                            newAnimais.push(animal);
                        }               
                    }
                }
            }


            //enviar o nome do responsavel dos animais





            res.status(200).json(newAnimais);

        }else{
            return res.status(403).json({ error: "O utilizador não é veterinário" });
        }    
    }
    catch(err){
        next(err);
    }  
};



// obter os eventos do veterinario
const getVeterinarioEventos = async(req, res, next) => {
    try{

        //obter o id do veterinario
        const { userId } = req.params;

        //obter o veterinario
        const userVeterinario = await User.findById(userId);  

        //Verifica se o user é de facto veterinário
        if(userVeterinario.role==='veterinario'){

            const eventos = await Evento.find({}).populate("animal");
            var newEventos = new Array();

            for(var i=0;i<eventos.length;i++){
                if(eventos[i].ativo===true && eventos[i].veterinario.toString()===userId.toString()){
                    newEventos.push(eventos[i]);
                }
            }

            

            res.status(200).json(newEventos);


        }else{
            return res.status(403).json({ error: "O utilizador não é veterinário" });
        }    
    }
    catch(err){
        next(err);
    }  
};


const newVeterinarioEventos = async(req, res, next) => {
    try {
        console.log("router.route('/Veterinarios')");
        console.log(".post(VeterinariosController.newVeterinarioEventos)");

        const body = req.body;

        //obter o id do user_veterinario
        const { userId } = req.params;


        const animalId = body.animal;
        const animal = await Animal.findById(animalId);

        if(animal.ativo === true){
            //Cria novo evento
            const evento = new Evento();
            evento.veterinario = userId;
            evento.animal = req.body.animal;
            evento.tipo = req.body.tipo;
            evento.data = req.body.data;
            evento.observacoes = req.body.observacoes;

            const result = await evento.save();

            res.status(200).json(result);
        }else{
            return res.status(403).json({ error: "Tentativa de registar treino para um animal inativo" });
        }

        

    } catch (err) {
        next(err);
    }
};

function textoAleatorio(tamanho)
{
    var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var aleatorio = '';
    for (var i = 0; i < tamanho; i++) {
        var rnum = Math.floor(Math.random() * letras.length);
        aleatorio += letras.substring(rnum, rnum + 1);
    }
    return aleatorio;
}

const deleteVeterinario = async(req,res,next) => {
    try{
        
        
        const randomText = textoAleatorio(8);
          
        //Obter o user através do id na bd
        const { userId } = req.params;
        const user = await User.findById(userId);
        
        if(user.role === "veterinario"){

            const veterinarioId = user.treinador;
            const veterinario = await Veterinario.findById(veterinarioId);
    

            if(user.ativo===true){
            
                //Colocar o estado como inativo 
                user.ativo = false;   
                //Colocar o campo nome a "cancelado"    
                user.name  = "CANCELADO"
                user.email = "CANCELADO"+randomText+"@C";
                await user.save();
                           

                res.status(200).json({ success: true , user, treinador});

    
            }else{
                return res.status(403).json({ error: "O utilizador não se encontra ativo." });
            }

        }else{
            return res.status(403).json({ error: "O utilizador não é veterinário" });
        }

    }catch(err){
        next(err);
    }
};



module.exports = {
    list : list,
    getVeterinarioAnimais,
    getVeterinarioEventos,
    newVeterinarioEventos,
    deleteVeterinario

};