const router     = require('express').Router();
const controller = require('./veterinarios.controller');
const auth       = require('../../middleware/auth.middleware');



router.get('/:userId/animais', auth.hasRole('admin','veterinario'), controller.getVeterinarioAnimais);
router.get('/:userId/eventos', auth.hasRole('admin','veterinario'), controller.getVeterinarioEventos);
router.post('/:userId/eventos', auth.hasRole('admin','veterinario'), controller.newVeterinarioEventos);
router.delete('/:userId', controller.deleteVeterinario);



module.exports = router;



