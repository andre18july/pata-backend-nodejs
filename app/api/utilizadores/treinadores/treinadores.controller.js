const utils = require('../../../components/utils');
const User = require('../../../models/User');
const Animal = require('../../../models/Animal');
const Treino = require('../../../models/Treino');
const Treinador = require('../../../models/Treinador');

function list(req, res){
    let query = {name : {$ne: 'anonymous'}};

    if(req.query.role == "treinador"){
        query.role = req.query.role;
    }
    User.find(query)
    .then(
        users => res.status(200).json(users))
    .catch(utils.handleError(req, res));
}


//devolve os animais todos do centro de treino / treinador
const getTreinadorAnimais = async(req, res, next) => {
    try{


        //obter o id do treinador
        const { userId } = req.params;

        //obter o treinador
        const user_treinador = await User.findById(userId);
        const treinador = await Treinador.findById(user_treinador.treinador).populate("treinos");


        //Verifica se o user é de facto treinador
        if(user_treinador.role==='treinador'){

            const animais = new Array();

            console.log("LENGTH: ", treinador.treinos.length);
 
            for (var i=0; i<treinador.treinos.length; i++) {
                const animal = await Animal.findById(treinador.treinos[i].animal).populate("responsavel");
                console.log("ANIMALLLLLLL: ", animal);

                if (animal.ativo === true) {
                                    
                    haveAnimal = false;
                    for(var j=0;j < animais.length;j++){

                         if(animal.id == animais[j].id){
                             haveAnimal = true;
                        }
                    }

                     if(!haveAnimal){
                        animais.push(animal);
                    }               
                }     
            }

            res.status(200).json(animais);

        }else{
            return res.status(403).json({ error: "O utilizador não é centro de treino" });
        }    
    }
    catch(err){
        next(err);
    }  
};



// obter os treinos do centro de treino / treinador
const getTreinadorTreinos = async(req, res, next) => {
    try{
        console.log("INNNNNNNNNNNN")

        //obter o id do treinador
        const { userId } = req.params;

        //obter o treinador
        const usertreinador = await User.findById(userId);  
        const treinador = await Treinador.findById(usertreinador.treinador).populate("treinos");

        if(usertreinador.role==='treinador'){
            const treinos = new Array();
            //populate animal
            for (var i=0; i<treinador.treinos.length; i++) {

                if(treinador.treinos[i].ativo === true){
                    const treino = await Treino.findById(treinador.treinos[i]).populate("animal");
                    treinos.push(treino);
                }
            }

            res.status(200).json(treinos);

        }else{
            return res.status(403).json({ error: "O utilizador não é centro de treino" });
        }   
   
    }
    catch(err){
        next(err);
    }  
};

const newTreinadorTreinos = async(req, res, next) => {
    try {
        console.log("router.route('/Treinadores')");
        console.log(".post(TreinadoresController.newTreinadorTreinos)");

        const body = req.body;

        //obter o id do user_treinador
        const { userId } = req.params;
        //obter o user treinador
        const usertreinador = await User.findById(userId);

        console.log("usertreinador.treinador:  ", usertreinador.treinador);

        //Adiciona o treino na lista de treinos do animal
        const animalId = req.body.animal;
        const animal = await Animal.findById(animalId);
        console.log("animalllll: ", animal);

        if(animal.ativo === true){


            //Cria novo treino
            const treino = new Treino();
            treino.treinador = userId;
            treino.animal = req.body.animal;
            treino.tipo = req.body.tipo;
            treino.desempenho = req.body.desempenho;
            treino.data = req.body.data;
            treino.observacoes = req.body.observacoes;

            console.log("treino: ", treino);

            //console.log("treino: ", treino);
            await treino.save();


            //console.log("Animal: ", animal);
            animal.treinos.push(treino);
            await animal.save();

            //Adiciona o treino na lista de treinos do treinador

            const treinador = await Treinador.findById(usertreinador.treinador);
            treinador.treinos.push(treino);
            await treinador.save();


            res.status(200).json(treino);


        } else{

            return res.status(403).json({ error: "Tentativa de registar treino para um animal inativo" });
        }

        


    } catch (err) {
        next(err);
    }
};

function textoAleatorio(tamanho)
{
    var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var aleatorio = '';
    for (var i = 0; i < tamanho; i++) {
        var rnum = Math.floor(Math.random() * letras.length);
        aleatorio += letras.substring(rnum, rnum + 1);
    }
    return aleatorio;
}

const deleteTreinador = async(req,res,next) => {
    try{
        
        
        //Obter o user através do id na bd

        const randomText = textoAleatorio(8);
          
        const { userId } = req.params;
        const user = await User.findById(userId);
        
        if(user.role === "treinador"){

            const treinadorId = user.treinador;
            const treinador = await Treinador.findById(treinadorId).populate('treinos');
    

            if(user.ativo===true){
            
                //Colocar o estado como inativo
                
                user.ativo = false;
                
                //Colocar o campo nome a "cancelado"
                
                user.name  = "CANCELADO"
                user.email = "CANCELADO"+randomText+"@C";

                await user.save();
                
                //Percorrer a lista dos seus animais e obter os ids dos animais
                //Colocar os estados dos animais a inativos e nomes dos animais a "Cancelado" de forma a não se identificar o utilizador
                
                
                for (var i=0; i<treinador.treinos.length; i++) {
                    
                    const treinoId = treinador.treinos[i];
                    const treino = await Treino.findById(treinoId);

                    treino.ativo = false;         
                    await treino.save();
                }
                

                res.status(200).json({ success: true , user, treinador});

    
            }else{
                return res.status(403).json({ error: "O utilizador não se encontra ativo." });
            }

        }else{
            return res.status(403).json({ error: "O utilizador não é treinador" });
        }

    }catch(err){
        next(err);
    }
};




/*
//seria utilizado para criar um novo animal para o centro de treino
const newTreinadorAnimal = async(req, res, next) => {
    try{

        
        const { userId } = req.params;
        const user = await User.findById(userId);
    
        if(user.role === "responsavelanimais"){
            console.log("req.params ", req.params)
            const newAnimal = new Animal(req.body);
            console.log("req.body", req.body);
            console.log("animal", newAnimal);
            //relacionar o user como sendo responsavel pelo animal
            newAnimal.responsavel = user;
            //gravar animal
            await newAnimal.save();
            //adicionar o animal aos users
            user.animais.push(newAnimal);
            //gravar user
            await user.save();
            //201 - algo foi criado
            res.status(201).json(newAnimal);
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
        
    } catch(err) {
        next(err);
    }
};
*/



module.exports = {
    list : list,
    getTreinadorAnimais,
    getTreinadorTreinos,
    newTreinadorTreinos,
    deleteTreinador,
    //newTreinadorAnimal

};