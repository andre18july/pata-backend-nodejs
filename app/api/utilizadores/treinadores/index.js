const router     = require('express').Router();
const controller = require('./treinadores.controller');
const auth       = require('../../middleware/auth.middleware');



router.get('/:userId/animais', auth.hasRole('admin','treinador'), controller.getTreinadorAnimais);
router.get('/:userId/treinos', auth.hasRole('admin','treinador'), controller.getTreinadorTreinos);
router.post('/:userId/treinos', auth.hasRole('admin','treinador'), controller.newTreinadorTreinos);
router.delete('/:userId', controller.deleteTreinador);
//router.post('/:userId/animais', auth.hasRole('admin','treinador'), controller.newTreinadorAnimal);


module.exports = router;



