const router     = require('express').Router();
const controller = require('./responsaveisanimais.controller');
const auth       = require('../../middleware/auth.middleware');

router.delete('/:userId', controller.deleteResponsavelAnimais);
router.get('/:userId/animais', auth.hasRole('admin','responsavelanimais'), controller.getUserAnimais);
router.post('/:userId/animais', auth.hasRole('admin','responsavelanimais'), controller.newUserAnimal);
router.get('/:userId/treinos', auth.hasRole('admin','responsavelanimais'), controller.getTreinosRespAnimal);
router.get('/:userId/eventos', auth.hasRole('admin','responsavelanimais'), controller.getEventosRespAnimal);
router.get('/:userId/ocorrencias', auth.hasRole('admin','responsavelanimais'), controller.getOcorrenciasRDA);
router.post('/:userId/ocorrencias', auth.hasRole('admin','responsavelanimais'), controller.newOcorrenciaRDA);




module.exports = router;



