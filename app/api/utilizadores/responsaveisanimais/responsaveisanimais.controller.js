const utils = require('../../../components/utils');
const User = require('../../../models/User');
const Animal = require('../../../models/Animal');
const ResponsavelAnimais = require('../../../models/ResponsavelAnimais');
const Treino = require('../../../models/Treino');
const Evento = require('../../../models/Evento');
const Ocorrencia = require('../../../models/Ocorrencia');
const TreinoRespAnimais = require('../../../models/TreinoRespAnimais');
const EventoRespAnimais = require('../../../models/EventoRespAnimais');
const OcorrenciaRespAnimais = require('../../../models/OcorrenciaRespAnimais');
const nodemailer = require('nodemailer');

function list(req, res){
    let query = {name : {$ne: 'anonymous'}};

    if(req.query.role == "responsavelanimais"){
        query.role = req.query.role;
    }
    User.find(query)
    .then(
        users => res.status(200).json(users))
    .catch(utils.handleError(req, res));
}

function textoAleatorio(tamanho)
{
    var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var aleatorio = '';
    for (var i = 0; i < tamanho; i++) {
        var rnum = Math.floor(Math.random() * letras.length);
        aleatorio += letras.substring(rnum, rnum + 1);
    }
    return aleatorio;
}

const deleteResponsavelAnimais = async(req,res,next) => {
    try{
        
        
        //Obter o user através do id na bd

        const randomText = textoAleatorio(8);
        
        const { userId } = req.params;
         const user = await User.findById(userId);
        

        if(user.role === "responsavelanimais"){

            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId).populate('animais');
    

            if(user.ativo===true){
            
                //Colocar o estado como inativo
                
                user.ativo = false;
                
                //Colocar o campo nome a "cancelado"
                
                user.name  = "CANCELADO"
                user.email = "CANCELADO"+randomText+"@C";

                await user.save();
                
                //Percorrer a lista dos seus animais e obter os ids dos animais
                //Colocar os estados dos animais a inativos e nomes dos animais a "Cancelado" de forma a não se identificar o utilizador
                
                
                for (var i=0; i<responsavelAnimais.animais.length; i++) {
                    
                    const animalId = responsavelAnimais.animais[i];
                    const animal = await Animal.findById(animalId);
  
                    animal.ativo=false;         
                    await animal.save();
                    //console.log("animal: ",animal);
                }
                

                res.status(200).json({ success: true , user, responsavelAnimais});

    
            }else{
                return res.status(403).json({ error: "O user já não se encontra ativo..." });
            }

        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }

    }catch(err){
        next(err);
    }
};


const getUserAnimais = async(req, res, next) => {
    try{
        

        
        const { userId } = req.params;
        //populate -> expande o conteudo dos objectos animais e grava todo o objecto em vez de gravar so os ids
        const user = await User.findById(userId);

        
        if(user.role === "responsavelanimais")
        {

            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId).populate('animais');
        


            var animais = new Array();


            //ver se os animais estão ativos
            console.log("animais user: ", responsavelAnimais.animais);
            console.log("animais length: ", responsavelAnimais.animais.length);


            for (var i=0; i<responsavelAnimais.animais.length; i++) {
                if (responsavelAnimais.animais[i].ativo === true) {

                    animais.push(responsavelAnimais.animais[i]);
                }
            }


            
            res.status(200).json(animais);
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
        
    }
    catch(err){
        next(err);
    }  
};

const newUserAnimal = async(req, res, next) => {
    try{
        //recebe o id do user
        const { userId } = req.params;
        const user = await User.findById(userId);

        var body = req.body;

        if(user.role === "responsavelanimais"){

            console.log("BINGOOOOOOOOOOOOOOOO");

            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId);

            if(body.fotoAnimal){
                if(body.fotoAnimal.changingThisBreaksApplicationSecurity === ""){

                    body.fotoAnimal=null;
                }
            }

            //console.log("req.params ", req.params)
            const newAnimal = new Animal(body);
            //console.log("req.body", req.body);
            //console.log("animal", newAnimal);
            //relacionar o user como sendo responsavel pelo animal
            newAnimal.responsavel = userId;
            //gravar animal
            await newAnimal.save();
            //adicionar o animal aos users
            responsavelAnimais.animais.push(newAnimal);
            //gravar user
            await responsavelAnimais.save();
            //201 - algo foi criado
            res.status(201).json(newAnimal);
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
    } catch(err) {
        next(err);
    }
};


const getTreinosRespAnimal = async(req, res, next) => {
    try{
        console.log("getTreinosRespAnimal");
        const { userId } = req.params;
        const user = await User.findById(userId);

        //valida se é mesmo o responsavel dos animais
        if(user.role === "responsavelanimais"){

            //obter o responsavel dos animais
            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId).populate("animais");

            //console.log("responsavel dos animais: ", responsavelAnimais);

            treinos = await Treino.find({});
            var newTreinos = new Array();

            //console.log("treinos: ", treinos);

            //percorrer a lista dos animais e verificar se o animal esta ativo
            for(var i=0; i<responsavelAnimais.animais.length; i++){
                
                if(responsavelAnimais.animais[i].ativo === true){
                    for(var j=0;j<treinos.length;j++){

                        if(treinos[j].animal.toString() == responsavelAnimais.animais[i]._id.toString()){

                            if(treinos[j].ativo === true){
                                newTreino = new TreinoRespAnimais();
                                newTreino._id = treinos[j]._id;
                                newTreino.data = treinos[j].data;
                                newTreino.treinador = treinos[j].treinador;
                                newTreino.tipo = treinos[j].tipo;
                                newTreino.animal = treinos[j].animal;
                                newTreino.desempenho = treinos[j].desempenho;
                                newTreino.observacoes = treinos[j].observacoes;
                                newTreino.ativo = treinos[j].ativo;

                                const animal = await Animal.findById(treinos[j].animal);
                                const userTreinador = await User.findById(treinos[j].treinador);
  

                                newTreino.nomeAnimal = animal.nome;
                                newTreino.nomeTreinador = userTreinador.name;

                                newTreinos.push(newTreino);
                            }    
                        }
                    }
                }
            }




            //percorre a lista dos treinos e compara cada treino com o animal
            res.status(200).json(newTreinos);
            
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
    } catch(err) {
        next(err);
    }
};






const getEventosRespAnimal = async(req, res, next) => {
    try{
        console.log("getEventosRespAnimal");
        const { userId } = req.params;
        const user = await User.findById(userId);

        //valida se é mesmo o responsavel dos animais
        if(user.role === "responsavelanimais"){

            //obter o responsavel dos animais
            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId).populate("animais");

            eventos = await Evento.find({}).populate("animal");
            var newEventos = new Array();

            //obter todos os eventos do user
            for(var x=0;x<eventos.length;x++){

                //valida evento
                if(eventos[x].ativo === true){
                    console.log("evento: ", eventos[x]);
                    //console.log("eventos[x].animal.responsavel: ", eventos[x].animal.responsavel)
                    //console.log("userId: ", userId)
    
                    if(eventos[x].animal.responsavel.toString() === userId){
                        newEvento = new EventoRespAnimais();
                        newEvento._id = eventos[x]._id;
                        newEvento.data = eventos[x].data;
                        newEvento.veterinario = eventos[x].veterinario;
                        newEvento.animal = eventos[x].animal;
                        newEvento.observacoes = eventos[x].observacoes;
                        newEvento.tipo = eventos[x].tipo;
                        newEvento.ativo = eventos[x].ativo;
    
                        const eventoAnimal = await Animal.findById(eventos[x].animal);
                        const eventoVeterinario = await User.findById(eventos[x].veterinario);
    
    
                        newEvento.nomeAnimal = eventoAnimal.nome;
                        newEvento.nomeVeterinario = eventoVeterinario.name;
    
                        newEventos.push(newEvento);
    
                    }

                }


            }


            //percorre a lista dos treinos e compara cada treino com o animal
            res.status(200).json(newEventos);
            
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
    } catch(err) {
        next(err);
    }
};









const getOcorrenciasRDA = async(req, res, next) => {
    try{
        const { userId } = req.params;
        const user = await User.findById(userId);

        //valida se é mesmo o responsavel dos animais
        if(user.role === "responsavelanimais"){

 
            ocorrencias = await Ocorrencia.find({});
            var newOcorrencias = new Array();

            for(var i=0; i<ocorrencias.length;i++){
                if(ocorrencias[i].ativo === true){

                    if(ocorrencias[i].responsavelAnimais!=null){
                        console.log("ocorrencias[i].responsavelAnimais: ", ocorrencias[i].responsavelAnimais.toString())
                        console.log("user: ", user._id.toString())
                        if(ocorrencias[i].responsavelAnimais == userId.toString()){
                            newOcorrencia = new OcorrenciaRespAnimais();
                            newOcorrencia._id = ocorrencias[i]._id;
                            newOcorrencia.data = ocorrencias[i].data;
                            newOcorrencia.tipo = ocorrencias[i].tipo;
                            newOcorrencia.animal = ocorrencias[i].animal;
                            newOcorrencia.responsavelAnimais = ocorrencias[i].responsavelAnimais;
                            newOcorrencia.descricao = ocorrencias[i].descricao;
                            newOcorrencia.localizacao = ocorrencias[i].localizacao;
                            newOcorrencia.lat = ocorrencias[i].lat;
                            newOcorrencia.long = ocorrencias[i].long;
                            newOcorrencia.estadoAnimal = ocorrencias[i].estadoAnimal;
                            newOcorrencia.tipoAnimal = ocorrencias[i].tipoAnimal;
                            newOcorrencia.racaAnimal = ocorrencias[i].racaAnimal;
                            newOcorrencia.fotoAnimal = ocorrencias[i].fotoAnimal;
                            newOcorrencia.contacto = ocorrencias[i].contacto;
                            newOcorrencia.agressor = ocorrencias[i].agressor;
                            newOcorrencia.notificada = ocorrencias[i].notificada;
                            newOcorrencia.ativo = ocorrencias[i].ativo;

                            const ocorrenciaAnimal = await Animal.findById(ocorrencias[i].animal);

                            newOcorrencia.nomeAnimal = ocorrenciaAnimal.nome;
                            
                            newOcorrencias.push(newOcorrencia);

                        }
                    }
                }
            }

            res.status(200).json(newOcorrencias);
            
        }else{
            return res.status(403).json({ error: "O utilizador não é responsavel por animais" });
        }
    } catch(err) {
        next(err);
    }
};



const newOcorrenciaRDA = async(req, res, next) => {


        try {
            console.log("router.route('/responsaveisanimais')");
            console.log(".post(ResponsavelAnimaisController.newOcorrenciaRDA)");
            //console.log("req.path", req.path);

            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);

            const body = req.body;

            const { userId } = req.params;
            const user = await User.findById(userId);
            const responsavelAnimaisId = user.responsavelAnimais;
            const responsavelAnimais = await ResponsavelAnimais.findById(responsavelAnimaisId);
            
            if(body.fotoAnimal){
                if(body.fotoAnimal.changingThisBreaksApplicationSecurity === ""){

                    body.fotoAnimal=null;
                }
            }
               

            const ocorrencia = await new Ocorrencia(body);

            ocorrencia.contacto = user.email;
            ocorrencia.responsavelAnimais = user;

            //valida se o animal é de algum responsavel dos animais em sistema
            if(body.animal){
                console.log("O animal tem dono no sistema");
                //obter o animal da BD
                const animal = await Animal.findById(body.animal);
                

                if(animal.ativo){

                    ocorrencia.tipoAnimal = animal.tipo;
                    ocorrencia.racaAnimal = animal.raca;
                    ocorrencia.animal = animal;

                }else{
                    return res.status(401).json({
                        error   : 'Animal inativo',
                        message : "O animal que associou a esta ocorrencia esta inativo no sistema."
                    });
                }
            
            }else{
                ocorrencia.animal=null;
                console.log("O animal nao tem dono no sistema");
            }

            //no caso de acidente
            if(body.tipo === "acidente"){
                if(!body.estadoAnimal){
                    return res.status(401).json({
                        error   : 'Estado tem de estar preenchido',
                        message : "No caso de ocorrencia de acidente o estado tem de estar preenchido."
                    });
                }  
            }


            //no caso de maus tratos
            if(body.tipo === "maustratos"){
                if(!body.agressor){
                    return res.status(401).json({
                        error   : 'Agressor do animal tem de estar preenchido',
                        message : "No caso de ocorrencia de maus tratos o agressor tem de estar preenchido."
                    });
                }
            }


            //geral
            if(!body.descricao){
                return res.status(401).json({
                    error   : 'Descricao da ocorrencia tem de estar preenchido',
                    message : "Descricao da ocorrencia tem de estar preenchido."
                });
            }
            if(!body.tipoAnimal && !ocorrencia.tipoAnimal){
                return res.status(401).json({
                    error   : 'Tipo de animal tem de estar preenchido',
                    message : "No caso de ocorrencia de perda o tipo de animal tem de estar preenchido."
                });
            }
            


            



            //enviar email a entidade competente
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'iseppata2018principal@gmail.com', // Your email id
                    pass: 'Andre!2018!' // Your password
                }
            });

            var text =  
            '\nOcorrência PATA:\n\n' +
            '\nID: ' + ocorrencia._id + 
            '\nTipo: ' + ocorrencia.tipo +
            '\nDescrição: ' + ocorrencia.descricao + 
            '\nId Animal: ' + ocorrencia.animal._id +
            '\nTipo do animal: ' + ocorrencia.tipoAnimal +
            '\nRaça do animal: ' + ocorrencia.racaAnimal +
            '\nEstado do animal: ' + ocorrencia.estadoAnimal +
            '\nLocalizacao: ' + ocorrencia.localizacao +
            '\nLatitude: ' + ocorrencia.lat +
            '\nLongitude: ' + ocorrencia.long +
            '\nContacto do requerente: ' + ocorrencia.contacto +
            '\nDescrição do agressor: ' + ocorrencia.agressor +
            '\n\n\nCumprimentos da PATA';

            var mailOptions = {
                from: '<iseppata2018principal@gmail.com>', // sender address
                to: "iseppata2018ec@gmail.com", // list of receivers
                subject: 'Ocorrência de '+ body.tipo + ' enviada pelo PATA', // Subject line
                text: text //, // plaintext body
            };


            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                    res.json({ error });
                } else {
                    enviado = true;
                    console.log('Message sent: ' + info.response);
                    res.json({ yo: info.response });
                };
                transporter.close();
            });

          


            ocorrencia.notificada=true;
            const result = await ocorrencia.save();
            res.status(200).json(ocorrencia);
 
            

        }
        catch (err) {
            next(err);
        }
    

};




module.exports = {
    list : list,
    deleteResponsavelAnimais,
    getUserAnimais,
    newUserAnimal,
    getTreinosRespAnimal,
    getEventosRespAnimal,
    getOcorrenciasRDA,
    newOcorrenciaRDA

};