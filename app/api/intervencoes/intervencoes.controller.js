const Evento = require('../../models/Evento');
const Intervencao = require('../../models/Intervencao');
const Animal = require('../../models/Animal');
const User = require('../../models/User');
const Veterinario = require('../../models/Veterinario');
const ResponsavelAnimais = require('../../models/ResponsavelAnimais');


module.exports = {


    //Get all intervencoes
    //router.get('/', controller.getIntervencoes);
    getIntervencoes: async function(req, res, next) {
        try {
            console.log("router.route('/Intervencoes')");
            console.log(".get(Intervencoes.controller.getIntervencoes)");
            //console.log("req.path", req.path);
            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);
   
            intervencoes = await Intervencao.find({});

            console.log(intervencoes);
            res.status(200).json(intervencoes);
            
        }
        catch (err) {
            next(err);
        }
    },


    //Get all intervencoes de um evento
    //router.get('/:eventoId', controller.getIntervencoesDeUmEvento);
    getIntervencoesDeUmEvento: async function(req, res, next) {
        try {
            console.log("router.route('/Intervencoes')");
            console.log(".get(Intervencoes.controller.getIntervencoesDeUmEvento)");
            //console.log("req.path", req.path);
            //console.log("REQ USER" , req.headers.authorization);
            //console.log("REQ: ", req);

            const role = req.params.userRole;
            const eventoId = req.params.eventoId;
   
            intervencoes = await Intervencao.find({});
            var newIntervencoes = new Array();
            
            const evento = await Evento.findById(eventoId);

            for(var i=0;i<intervencoes.length;i++){

                if(role==='admin'){
                    if(intervencoes[i].evento.toString()===eventoId.toString()){
                        newIntervencoes.push(intervencoes[i]);
                    }
                }else if(role==='veterinario'){
                    if(intervencoes[i].ativo === true && intervencoes[i].evento.toString()===eventoId.toString()){
                        newIntervencoes.push(intervencoes[i]);
                    }
                }else if(role==='responsavelanimais'){
                    if(intervencoes[i].ativo === true && intervencoes[i].evento.toString()===eventoId.toString()){
                        newIntervencoes.push(intervencoes[i]);
                    }
                }
            }


            console.log(newIntervencoes);
            res.status(200).json(newIntervencoes);
            
        }
        catch (err) {
            next(err);
        }
    },


    getIntervencaoId: async function(req, res, next) {
        try{
            console.log("getIntervencaoId");
            
            const { intervencaoId } = req.params;
            console.log("intervencaoId: ", intervencaoId);
            const intervencao = await Intervencao.findById(intervencaoId);
            console.log("intervencao: ", intervencao);

            res.status(200).json(intervencao);
            

        }catch(err){
            next(err);
        }
    },



    //new intervencao
    //router.route('/Intervencoes')
    //.post(IntervencoesController.newIntervencao);
    newIntervencao: async function(req, res, next) {
        try {
            console.log("router.route('/Intervencoes')");
            console.log(".post(IntervencoesController.newIntervencao)");
            const body = req.body;

            //Cria nova intervencao
            const intervencao = await new Intervencao(body);
            console.log("intervencao: ", intervencao);

            const evento = await Evento.findById(intervencao.evento);
            console.log("evento: ", evento);


            if(intervencao.data < evento.data){
                return res.status(403).json({ error: "A data da intervenção tem de ser superior à do evento" });
            }else{
                await intervencao.save();
            }

            res.status(200).json(intervencao);      
        } catch (err) {
            next(err);
        }
    },


    changeIntervencao: async function(req, res, next) {
        try{

            const { intervencaoId } = req.params;
            const intervencaoNova = req.body;


            const evento = await Evento.findById(intervencaoNova.evento);
            const dataEvento = evento.data;
            

            //valida a data
            if(intervencaoNova.data < dataEvento){
                return res.status(403).json({ error: "A data da intervenção tem de ser superior à do evento" });
            }else{
                const result = await Intervencao.findByIdAndUpdate(intervencaoId, intervencaoNova);
            }

            res.status(200).json({ success: true });

        }catch(err){
            next(err);
        }
    },



    //Delete Intervencao
    //router.route('/Intervencoes/idIntervencao')
    //.delete(IntervencoesController.delIntervencaoId);
    delIntervencaoId: async function(req, res, next) {
        try {
            console.log("router.route('/Intervencoes/idIntervencao')");
            console.log(".post(IntervencoesController.delIntervencaoId)");

            const { intervencaoId } = req.params;
            console.log("intervencaoId: ", intervencaoId);
            const intervencao = await Intervencao.findById(intervencaoId);

            if(intervencao.ativo)
            {
                console.log("intervencao: ", intervencao);
                intervencao.ativo = false;

                await intervencao.save();

                res.status(200).json({ success: true });
            }else{
                return res.status(403).json({ error: "Tentativa de anonimizar uma intervencao já anonimizada" });
            }

        } catch (err) {
            next(err);
        }
    },



    deleteIntervencaoFromDb: async function(req, res, next){
        try{
            const { intervencaoId } = req.params;
            const intervencao = await Intervencao.findById(intervencaoId);

            await intervencao.remove();
            res.status(200).json({ success: true });     
        }catch(err){
            next(err);
        }
    },


   

}

