const router     = require('express').Router();
const controller = require('./intervencoes.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', controller.getIntervencoes);

router.get('/:eventoId/:userRole', controller.getIntervencoesDeUmEvento);
router.post('/', auth.hasRole('admin', 'veterinario'), controller.newIntervencao);


router.get('/:intervencaoId/intervencao/detalhe', auth.hasRole('admin','responsavelanimais','veterinario'), controller.getIntervencaoId);
router.put('/:intervencaoId', auth.hasRole('admin','veterinario'), controller.changeIntervencao);
router.delete('/:intervencaoId', auth.hasRole('admin','veterinario'), controller.delIntervencaoId);

router.delete('/:intervencaoId/fromdb', auth.hasRole('admin'), controller.deleteIntervencaoFromDb);





module.exports = router;

