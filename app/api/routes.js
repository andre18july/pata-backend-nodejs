const router = require('express').Router(); // get an instance of the express Router
const auth = require('./middleware/auth.middleware');
// ROUTES FOR OUR API
// =============================================================================
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Hooray! Welcome to our api!' });
});

router.use('/auth', require('./auth'));
router.use('/chart', require('./chart'));
router.use('/utilizadores',  auth.isAuthenticated(), require('./utilizadores'));
router.use('/responsaveisanimais',  auth.isAuthenticated(), require('./utilizadores/responsaveisanimais'));
router.use('/treinadores',  auth.isAuthenticated(), require('./utilizadores/treinadores'));
router.use('/veterinarios',  auth.isAuthenticated(), require('./utilizadores/veterinarios'));
router.use('/animais', auth.isAuthenticated(), require('./animais'));
router.use('/treinos', auth.isAuthenticated(), require('./treinos'));
router.use('/eventos', auth.isAuthenticated(), require('./eventos'));
router.use('/intervencoes', auth.isAuthenticated(), require('./intervencoes'));
router.use('/ocorrencias', require('./ocorrencias'));

module.exports = router;