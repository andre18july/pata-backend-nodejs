const mongoose   = require('mongoose');
const bodyParser = require('body-parser');
const morgan     = require('morgan');
const config     = require('./environment');
const cors       = require('cors');
const websocket  = require('../components/websocket');
const scheduler  = require('../components/scheduler');
//const CronJob = require('cron').CronJob;

/*
const job = new CronJob('* * * * * *', () => {
    console.log('teste cron')
})
job.start()
*/

function configureMongoose(){
    mongoose.connect(config.mongoose.uri, {useMongoClient: true}); // connect to our database
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    
    if(config.env !== 'test'){
        db.on('error', console.error.bind(console, 'Connection error:'));
        db.on('connected', console.info.bind(console, 'Successfully connected to mongoose database'));
    }
}

module.exports = (app) => {   
    configureMongoose();
    
    // configure app to use bodyParser()
    // this will let us get the data from a POST


    //app.use(bodyParser.urlencoded({ extended: true }));
    //app.use(bodyParser.json());

    app.use(bodyParser.urlencoded({
        extended: true,
        limit: '2mb',
        parameterLimit: 100000
        }))
      
       app.use(bodyParser.json({
        limit: '2mb',
        parameterLimit: 100000
       }))

    // Enable cors requests
    app.use(cors());

    if(config.env !== 'test') {
        //don't show the log when it is test
        //use morgan to log at command line
        app.use(morgan('dev'));

        // Begin the schedule
        scheduler.begin();
    }
};