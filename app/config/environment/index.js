const _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
let all = {
    env: env,

    // Server port
    port: process.env.PORT || 8080,

    // Mongoose connection 
    mongoose: {

        //testes
        //uri: "mongodb://pataadmin:pataadmin@ds064649.mlab.com:64649/pata2018test"

        //prod
        uri: "mongodb://admin:Pata!2018@ds062178.mlab.com:62178/pata2018"
    },

    jwt: {
        secret: "fSk35bzq6KutR0dQVKTL",
        issuer: "http://projeto.arqsi.local",
        audience: "Everyone"
    },

    mail: {
        host: 'smtp.gmail.com',
        port: 587,
        username: 'xxx@gmail.com',
        password: 'passwordTeste'
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`)
);