const seeder = require('mongoose-seeder');
const data = require('./data.json');
const config = require('../app/config/environment');
const mongoose = require('mongoose');

//Register the models in mongoose
require('../app/models/User');
require('../app/models/Animal');

function executeSeeder() {


    mongoose.connect(config.mongoose.uri, { useMongoClient: true }); // connect to our database
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;


    db.on('connected', function() {
        seeder.seed(data).then(function(dbData) {
            console.log('Seeder successfully executed');
            console.log(extractData(dbData));
            db.close();
        }).catch(function(err) {
            console.log('Error executing seeder', err);
            db.close();
        });

    });
}

function extractData(obj) {
    let data = {};
    for (var key in obj.users) {
        data[key] = obj.users[key]._id;
    }
    return data;
}

executeSeeder();